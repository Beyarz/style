# Style picker

Allow other people to suggest your fashion

## Getting started

`yarn`

## Requirements

- Node
- Yarn

## Extensions

- [Better Comments](https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments)
